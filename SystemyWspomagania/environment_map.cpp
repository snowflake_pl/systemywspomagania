#include "environment_map.h"

#include <field.h>

namespace environment {
namespace constant {
}

size_t map::width() const
{
	return map_.size();
}

size_t map::height() const
{
	return map_.empty() ? 0 : map_.at(0).size();
}

inline field infinite_drain(const field& f)
{
	return field{f.x_, f.y_, 0, f.height(), 0, 0, true};
}

map::map(std::initializer_list<row> list) : map_(list.begin(), list.end())
{}

map::map(size_t count) : map(count, {})
{}

map::map(size_t count, row && row_) : map_(count, row_)
{}

map::map(const std::vector<std::vector<size_t>>& heights) : map_{}
{
	const size_t width = heights.size();
	const size_t height = heights.empty() ? 0 : heights.at(0).size();

	for (size_t i = 0; i <width; ++i) {
		map_.push_back(environment::row{});
		for (size_t j = 0; j < height; ++j) {
			map_[i].push_back(environment::field(i, j, 1,
			                                    heights.at(i).at(j),
			                                    0,
			                                    0,
			                                    0));
		}
	}
}

field& map::operator[](const field& fld)
{
	return map_[fld.x_][fld.y_];
}


//inline
field map::at(size_t i, size_t j) const
{
    return map_.at(i).at(j);
}

//inline
row& map::operator[](size_t i)
{
    return map_[i];
}

//inline
row_iterator map::begin()
{
    return map_.begin();
}

//inline
row_iterator map::end()
{
    return map_.end();
}

//inline
row_const_iterator map::begin() const
{
    return map_.begin();
}

//inline
row_const_iterator map::end() const
{
    return map_.end();
}

//inline
size_t map::size() const
{
	return map_.size();
}

bool map::empty() const
{
	return size() == 0;
}

void map::push_back(row&& row_)
{
    map_.push_back(std::move(row_));
}

field map::left_of(const field& f) const
{
    return  !is_on_left_edge(f) ? at(f.x_ - 1, f.y_) : infinite_drain(f);
}

field map::right_of(const field& f) const
{
    return !is_on_right_edge(f) ? at(f.x_ + 1, f.y_) : infinite_drain(f);
}

field map::above(const field& f) const
{
    return !is_on_top(f) ? at(f.x_, f.y_ - 1) : infinite_drain(f);
}

field map::below(const field& f) const
{
    return !is_at_the_bottom(f) ? at(f.x_, f.y_ + 1) : infinite_drain(f);
}

inline bool map::is_on_left_edge(const field& f) const
{
    return f.x_ == 0;
}

inline bool map::is_on_right_edge(const field& f) const
{
    return f.x_ >= (width() - 1);
}

inline bool map::is_on_top(const field& f) const
{
    return f.y_ == 0;
}

inline bool map::is_at_the_bottom(const field& f) const
{
	return f.y_ >= (height() - 1);
}

field& map::right_of(const field& f)
{
	auto ab = const_cast<const map*>(this)->right_of(f);
	return (*this)[ab];
}

field& map::above(const field& f)
{
	auto ab = const_cast<const map*>(this)->above(f);
	return (*this)[ab];
}

field& map::left_of(const field& f)
{
	auto ab = const_cast<const map*>(this)->left_of(f);
	return (*this)[ab];
}

field& map::below(const field& f)
{
	auto ab = const_cast<const map*>(this)->below(f);
	return (*this)[ab];
}

void map::add_river(const std::vector<std::pair<size_t, size_t> >& river, double water_volume)
{
	size_t x, y;
	auto unwrapper = std::tie(x, y);

	for (const auto& field: river) {
		unwrapper = field;
		auto& f = map_[x][y];
		f.is_river = true;
		f.set_value(0);
		f.add_water(water_volume);
	}
}

void map::add_values(std::vector<std::vector<size_t>>&& values)
{
	const size_t width = values.size();
	const size_t height = values.empty() ? 0 : values.at(0).size();

	for (size_t x = 0; x < width; ++x) {
		for (size_t y = 0; y < height; ++y) {
			map_[x][y].set_value(values.at(x).at(y));
		}
	}
}

} // napespace environment
