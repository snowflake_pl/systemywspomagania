#include <abstract_optimizable_stub.h>

#include <cmath>
#include <random>
namespace utility {
static std::random_device random_device;
static std::default_random_engine random_engine(random_device());
} // namespace utility


double abstract_optimizable_stub::cost() const
{
    double cost = std::pow(x,4) -16 * std::pow(x,2) + 5*x;
    return cost/2.0;
}

void abstract_optimizable_stub::next_state(double temperature)
{
    std::normal_distribution<double> randomizer(optimal_x, temperature);
    auto step = randomizer(utility::random_engine);
    x = step;
}

void abstract_optimizable_stub::save_optimal_state()
{
    optimal_x = x;
}


void abstract_optimizable_stub::request_interrupt()
{
}