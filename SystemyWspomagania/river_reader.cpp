#include "river_reader.h"
#include <png++/image.hpp>

namespace {
//std::ostream& operator<<(std::ostream& out, const png::rgb_pixel& px)
//{
//	return out <<"(" <<static_cast<int>(px.red)
//	           <<", "<<static_cast<int>(px.green)
//	           <<", "<<static_cast<int>(px.blue)<<") ";
//}
bool operator==(const png::rgb_pixel& a, const png::rgb_pixel& b)
{
	return (a.blue == b.blue && a.green == b.green && a.red == b.red);
}
png::rgb_pixel read_blue_pixel()
{
	png::image<png::rgb_pixel> image("./blue_pixel.png");
	return image.get_pixel(0,0);
}
}

namespace constant {
static const png::rgb_pixel blue_pixel = read_blue_pixel();
}


inline bool is_river(const png::rgb_pixel& pix)
{
	return pix == constant::blue_pixel;
}

river_reader::river_reader(const path& file_path) : river_path(file_path)
{
	png::image<png::rgb_pixel> image(river_path.c_str());

	for (size_t x = 0; x < image.get_width(); ++x) {
		for (size_t y = 0; y < image.get_height(); ++y) {
			if (is_river(image.get_pixel(x, y))) {
				river_fields.push_back({x, y});
			}
		}
	}
	river_fields.shrink_to_fit();
}

std::vector<std::pair<size_t, size_t>> river_reader::get_river_map() const
{
	return river_fields;
}


