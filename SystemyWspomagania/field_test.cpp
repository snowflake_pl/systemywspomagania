#include <gtest/gtest.h>

#include <field.h>

namespace environment {

class field_test : public ::testing::Test
{
protected:
	void validate_field(const field& field,
	                    size_t x,
	                    size_t y,
	                    double value,
	                    double height,
	                    bool is_river)
	{
		EXPECT_EQ(height, field.height());
		EXPECT_EQ(x, field.x_);
		EXPECT_EQ(y, field.y_);
		EXPECT_EQ(value, field.value());
		EXPECT_EQ(is_river, field.is_river);
	}

	field sut;
};

TEST_F(field_test, adding_water_increases_water_height)
{
	EXPECT_EQ(0, sut.water_height());

	sut.add_water(100);

	EXPECT_GT(sut.water_height(), 0);
}

TEST_F(field_test, adding_sand_increases_effective_height)
{
	EXPECT_EQ(0, sut.height());

	sut.set_sand_height(100);

	EXPECT_EQ(100, sut.height());
}
}
