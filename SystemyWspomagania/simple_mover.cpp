#include "simple_mover.h"
#include <environment_map.h>
#include <field.h>

namespace utility {
static std::random_device random_device;
static std::default_random_engine random_engine(random_device());
} // namespace utility

namespace constant {
constexpr double sandbag_height = 0.5; // meter
constexpr double sandbag_length = 1.0;// meter
constexpr double cell_side = environment::field::dx;
constexpr double stacks_per_side = (cell_side / sandbag_length);
} // namespace constant

namespace {
size_t bags_per_cell(double weir_height)
{
	size_t stack_size = std::ceil(weir_height/constant::sandbag_height);
	return stack_size * constant::stacks_per_side;
}
} // namespace unnamed

simple_mover::simple_mover(const size_t sandbag_count, environment::map& map,
                           const std::vector<std::pair<size_t, size_t>>& river,
                           long double weir_height):
    sandbag_count(sandbag_count),
    map(map),
    randomizer(0, river.size()),
    weir_center(randomizer(utility::random_device)),
    optimal_center(weir_center),
    number_of_fields_to_cover(0),
    cells_per_side(0),
    river_fields(river),
    required_weir_height(weir_height),
    fields_embanket(0),
    unwrapper(std::tie(x, y)),
    can_build_more_up(true),
    can_build_more_down(true)
{
	if (map.empty() || river_fields.empty() || sandbag_count == 0) {
		throw std::runtime_error("Cannot create Simple Mover with empty map, "
		                         "empty river or no sandbags");
	}
}

void simple_mover::find_new_weir_center(double temperature)
{
	weir_center = next_point_finder(optimal_center, river_fields.size(), temperature,
	                                utility::random_engine);
}

void simple_mover::calculate_weir_dimmensions()
{
	number_of_fields_to_cover = sandbag_count / bags_per_cell(required_weir_height);
}

void simple_mover::perform_move(double temperature)
{
	if (required_weir_height == 0) {
		return;
	}
	reset_counters();
	find_new_weir_center(temperature);
	calculate_weir_dimmensions();
	build_weir();
}

void simple_mover::build_weir()
{
	embant_weir_center_from_all_sides();

	size_t top_right_index = weir_center + 1;
	size_t bottom_left_index = weir_center;

	while (can_continue()) {
		try_building_up_the_river(top_right_index);
		try_building_down_the_river(bottom_left_index);
	}
}

void simple_mover::embant_weir_center_from_all_sides()
{
	unwrapper = river_fields.at(weir_center);
	embankt_field(map[x][y]);
}

bool simple_mover::can_continue() const
{
	return (fields_embanket < number_of_fields_to_cover)
	        && (can_build_more_up || can_build_more_down);
}

void simple_mover::try_building_up_the_river(size_t& top_right_index)
{
	if (top_right_index < river_fields.size()) {
		unwrapper = river_fields.at(top_right_index++);
		embankt_field(map[x][y]);
	} else {
		can_build_more_up = false;
	}
}

void simple_mover::try_building_down_the_river(size_t& bottom_left_index)
{
	if (bottom_left_index) {
		unwrapper= river_fields.at(--bottom_left_index);
		embankt_field(map[x][y]);
	} else {
		can_build_more_down = false;
	}
}

void simple_mover::embankt_field(environment::field& field)
{
	embankt_top_of(field);
	embankt_bottom_of(field);
	embankt_right_of(field);
	embankt_left_of(field);
}

void simple_mover::embankt_left_of(environment::field& field)
{
	embankt(map.left_of(field));
}

void simple_mover::embankt_right_of(environment::field& field)
{
	embankt(map.right_of(field));
}

void simple_mover::embankt_bottom_of(environment::field& field)
{
	embankt(map.below(field));
}

void simple_mover::embankt_top_of(environment::field& field)
{
	embankt(map.above(field));
}

void simple_mover::embankt(environment::field& field)
{
	if (field.is_river || fields_embanket >= number_of_fields_to_cover) {
		return;
	}
	++fields_embanket;
	field.set_sand_height(required_weir_height);
}

void simple_mover::reset_counters()
{
	fields_embanket = 0;
	can_build_more_down = can_build_more_up = true;
}

void simple_mover::save_optimal()
{
	optimal_center = weir_center;
}
