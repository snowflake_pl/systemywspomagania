#include "two_sided_mover.h"
#include <environment_map.h>
#include <field.h>

namespace utility {
static std::random_device random_device;
static std::default_random_engine random_engine(random_device());
}

namespace constant {
constexpr double sandbag_height = 0.5; // meter
constexpr double sandbag_length = 1.0;// meter
constexpr double cell_side = environment::field::dx;
constexpr double stacks_per_side = (cell_side / sandbag_length);
} // namespace constant

namespace {
size_t bags_per_cell(double weir_height)
{
	size_t stack_size = std::ceil(weir_height/constant::sandbag_height);
	return stack_size * constant::stacks_per_side;
}
} // namespace unnamed

two_sided_mover::two_sided_mover(size_t sandbag_count, environment::map& map,
                                 const std::vector<std::pair<size_t, size_t>>& river,
	                             long double weir_height) :
    sandbag_count{{false, sandbag_count / 2}, {true, sandbag_count / 2}},
    map(map),
    river(river),
    randomizer(0, river.size()),
    center{{false, randomizer(utility::random_engine)}, {true, randomizer(utility::random_engine)}},
    optimal(center),
    required_weir_height(weir_height),
    unwrapper(std::tie(x, y)),
    can_go_higher{{true, true}, {false, true}},
    can_go_lower{{true, true}, {false, true}}
{
	if (map.empty() || river.empty() || sandbag_count == 0) {
		throw std::runtime_error("Cannot create two sided mover with empty map, "
		                         "empty river or no sandbags");
	}
}

void two_sided_mover::perform_move(double temperature)
{
	if (required_weir_height == 0) {
		return;
	}
	reset_counters();
	center[true] = find_new_weir_center(optimal[true], temperature);
	center[false] = find_new_weir_center(optimal[false], temperature);
	cells_to_cover[true] =  calculate_weir_dimmensions(sandbag_count[true]);
	cells_to_cover[false] = calculate_weir_dimmensions(sandbag_count[false]);
	build_weir(center[true], true);
	build_weir(center[false], false);
}

void two_sided_mover::save_optimal()
{
	optimal = center;
}

size_t two_sided_mover::find_new_weir_center(size_t center, double temperature)
{
	return next_point_finder(center, river.size(), temperature,
	                         utility::random_engine);
}

size_t two_sided_mover::calculate_weir_dimmensions(size_t sandbag_count)
{
	return sandbag_count / bags_per_cell(required_weir_height);
}

void two_sided_mover::build_weir(size_t center, bool right)
{
	embankt_weir_center(center, right);

	size_t top_right_index = center + 1;
	size_t bottom_left_index = center;

	while (can_continue(right)) {
		try_building_up(top_right_index, right);
		try_building_downward(bottom_left_index, right);
	}
}

bool two_sided_mover::can_continue(bool right) const
{
	return (can_go_higher.at(right) || can_go_lower.at(right))
	        && can_build_up_more_cells(right);
}

bool two_sided_mover::can_build_up_more_cells(bool right) const
{
	return fields_embanket.at(right) <= cells_to_cover.at(right);
}

void two_sided_mover::try_building_up(size_t& top_right_index, bool right)
{
	if (top_right_index < river.size()) {
		unwrapper = river.at(top_right_index++);
		embankt_field(map[x][y], right);
	} else {
		can_go_higher[right] = false;
	}
}

void two_sided_mover::try_building_downward(size_t& bottom_left_index, bool right)
{
	if (bottom_left_index) {
		unwrapper= river.at(--bottom_left_index);
		embankt_field(map[x][y], right);
	} else {
		can_go_lower[right] = false;
	}
}

void two_sided_mover::embankt_weir_center(size_t center, bool right)
{
	unwrapper = river.at(center);
	embankt_field(map[x][y], right);
}

void two_sided_mover::embankt_field(environment::field& field, bool right)
{
	if (right) {
		embankt_right_of(field);
		embankt_top_of(field);
	} else {
		embankt_left_of(field);
		embankt_bottom_of(field);
	}
}

void two_sided_mover::embankt_left_of(environment::field& field)
{
	embankt(map.left_of(field), false);
}

void two_sided_mover::embankt_right_of(environment::field& field)
{
	embankt(map.right_of(field), true);
}

void two_sided_mover::embankt_bottom_of(environment::field& field)
{
	embankt(map.below(field), false);
}

void two_sided_mover::embankt_top_of(environment::field& field)
{
	embankt(map.above(field), true);
}

void two_sided_mover::reset_counters()
{
	fields_embanket[true] = fields_embanket[false] = 0;
	can_go_higher = can_go_lower = {{true, true}, {false, true}};
}

void two_sided_mover::embankt(environment::field& field, bool right)
{
	if (field.is_river || !can_build_up_more_cells(right)) {
		return;
	}
	++fields_embanket[right];
	field.set_sand_height(required_weir_height);
}
