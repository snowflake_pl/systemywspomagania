#include "lisflood.h"

#include <iomanip>
#include <ios>
#include <random>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <field.h>
#include <map_reader.h>
#include <river_reader.h>
#include <abstract_sandbag_mover.h>
#include <fstream>

namespace utility {
static std::random_device random_device;
static std::default_random_engine random_engine(random_device());
} // namespace utility

namespace constant {
constexpr long double n = 0.15;
constexpr long double hflow_power = 5.0/3.0;
constexpr long double dt = 250;
constexpr long double dx = environment::field::dx;
constexpr long double dy = environment::field::dy;
constexpr long double cell_area = environment::field::cell_area;
constexpr long double outgoing = -1.0;
constexpr long double incoming = 1.0;
constexpr long double water_height = 3.0 * cell_area;
} // namespace constant

lisflood::lisflood(std::unique_ptr<abstract_sandbag_mover> mover,
                   environment::map& map_,
                   const std::vector<std::pair<size_t, size_t> >& riv,
                   std::function<png::rgb_pixel(size_t)> pixel_from_height,
                   std::function<double(const png::rgb_pixel&)> height_from_pixel,
                   double water_volume, size_t iterations,
                   std::vector<std::vector<size_t>>&& values) :
    remaining_bags(10),
    map{map_},
    optimal_state{},
    initial_map{},
    pixel_from_height(std::move(pixel_from_height)),
    height_from_pixel(std::move(height_from_pixel)),
    mover(std::move(mover)),
    river(riv),
    file("big_map_costs.txt"),
    iterations(iterations)
{
	if (!this->mover) {
		throw std::runtime_error("nie ma movera ;(");
	}

	map.add_river(river, water_volume);
	map.add_values(std::move(values));
	initial_map = map;
}

lisflood::~lisflood()
{
}

void lisflood::print_map()
{
	std::cout << "**************" << std::endl;
	double water(0);
	for (const auto& row : map) {
		for (const auto& field : row) {
			std::cout << std::fixed;
			std::cout.width(7);
			std::cout.precision(3);
			std::cout <<  field.water_height() - field.height();
			water += field.water_height() - field.height();
		}
		std::cout << std::endl;
	}
	std::cout << "**************" << std::endl;
	std::cout << "woda : "<< water << std::endl;
}

double lisflood::cost() const
{
	cost_ = 0.0;
	for (const auto& row : map) {
		for (const auto& field : row) {
			cost_ += field.value();
		}
	}
	return cost_;
}

void lisflood::calculate_flow()
{
	std::vector<std::vector<double>> delta_map;

	for (const auto& row : map) {
		std::vector<double> delta_row;
		for (const auto& field : row) {
			delta_row.push_back(delta_volume(field));
		}
		delta_map.push_back(std::move(delta_row));
	}

	for (auto& row : map) {
		for (auto& field : row) {
			field.add_water(delta_map.at(field.x_).at(field.y_));
		}
	}
}

inline
double lisflood::delta_volume(const environment::field& f) const
{
	return Q(f, map.above(f)) + Q(f, map.below(f)) + Q(f, map.left_of(f)) + Q(f, map.right_of(f));
}

inline
double lisflood::Q(const environment::field& f1, const environment::field& f2) const
{
	const auto Q = friction_slope(f1, f2) * water_slope(f1, f2) * constant::dy;
	const auto flow_limited_result = flow_limmiter(Q, f1, f2);
	return direction(f1, f2) * flow_limited_result;;
}

inline
double lisflood::friction_slope(const environment::field& f1, const environment::field& f2) const
{
	return std::pow(hflow(f1, f2), constant::hflow_power) / constant::n;
}

inline
double lisflood::water_slope(const environment::field& f1, const environment::field& f2) const
{
	return std::sqrt(std::abs(f1.water_height() - f2.water_height()) / constant::dx);
}

void lisflood::interrupt()
{
	throw std::runtime_error("Interrupted");
}

inline
double lisflood::flow_limmiter(long double res,
                               const environment::field& f1,
                               const environment::field& f2) const
{
	return std::min(res, constant::cell_area
	                * std::abs(f1.water_height() - f2.water_height())
	                / (4.0 * constant::dt));
}

inline
double lisflood::direction(const environment::field& f1, const environment::field& f2) const
{
	using namespace constant;
	return std::isless(f2.water_height(), f1.water_height()) ? outgoing : incoming;
}

inline
double lisflood::hflow(const environment::field& f1, const environment::field& f2) const
{
	return std::max(f1.water_height(), f2.water_height()) - std::max(f1.height(), f2.height());
}

void lisflood::next_state(double temperature)
{
	if (interrupt_requested) {
		interrupt();
	}
	map = initial_map;
	cost();
	mover->perform_move(temperature);
	for (size_t i = 0; i < iterations; ++i)  {
		calculate_flow();
	}
	std::cerr <<"iteration temp: "<<temperature<<std::endl;
}

png::rgb_pixel read_blue_pixel()
{
	png::image<png::rgb_pixel> image("./blue_pixel.png");
	return image.get_pixel(0,0);
}

png::rgb_pixel read_black_pixel()
{
	png::image<png::rgb_pixel> image("./black_pixel.png");
	return image.get_pixel(0,0);
}


png::rgb_pixel lisflood::height_to_pixel(const environment::field& f)
{
	static const png::rgb_pixel blue = read_blue_pixel();
	static const png::rgb_pixel black = read_black_pixel();

	if (f.sand_height()) {
		return black;
	}
	if (f.is_river || f.is_flooded()) {
		return blue;
	}
	return pixel_from_height(f.height());
}

void lisflood::save_map_as_image(const std::string& filename)
{
	static size_t counter = 100;

	std::string name = "./images/"+ filename + "_"
	                   + std::to_string(counter++)
	                   + "cost_" + std::to_string(static_cast<size_t>(cost_)) +
	                   + ".png";
	png::image<png::rgb_pixel> output(map.width(), map.height());

	for (const auto& row : map) {
		for (const auto& f : row) {
			output.set_pixel(f.x_, f.y_, height_to_pixel(f));
		}
	}
	output.write(name.c_str());
}

void lisflood::save_optimal_state()
{
	optimal_state = map;
	mover->save_optimal();
	save_map_as_image("optimal");
	file << "optimal cost: "<<cost_ <<std::endl;
}


void lisflood::request_interrupt()
{
	interrupt_requested = true;
}
