#include <gtest/gtest.h>

#include <simple_mover.h>
#include <environment_map.h>
#include <map_reader.h>
#include <river_reader.h>
#include <field.h>

#include <vector>

class simple_mover_test : public ::testing::Test
{
public:
	simple_mover_test() :
	    mapper("./legend", "./mapa_terenu3.png"),
	    map(mapper.read_map_file()),
	    original_map{},
	    riv("./rzeka.png"),
	    river(riv.get_river_map()),
	    sut(1000, map, river, 1)
	{
		map.add_river(river, 1);
		original_map = map;
	}

protected:
	map_reader mapper;
	environment::map map;
	environment::map original_map;
	river_reader riv;
	std::vector<std::pair<size_t, size_t>>river;
	simple_mover sut;

	png::rgb_pixel pixel_from_height(const environment::field& f)
	{
		if (f.sand_height()) {
			return {};
		}
		if (f.is_river) {
			return {100, 149, 237};
		}

		return mapper.convert_to_pix(f.height());
	}

	void save_map_as_image(const std::string& filename)
	{
		static size_t counter = 100;

		std::string name = "./images/"+ filename + "_"+ std::to_string(counter++) + ".png";
		png::image<png::rgb_pixel> output(map.width(), map.height());

		for (const auto& row : map) {
			for (const auto& f : row) {
				output.set_pixel(f.x_, f.y_, pixel_from_height(f));
			}
		}
		output.write(name.c_str());
	}
};


TEST_F(simple_mover_test, can_move_the_weir)
{
	size_t iters = 100;
	for (size_t counter = 0; counter < iters; ++counter) {
		sut.perform_move(1.0);
		save_map_as_image("simple_mover");
		map = original_map;
		std::cerr <<counter << std::endl;
	}
}

TEST_F(simple_mover_test, can_move_the_weir_and_saving_optimal)
{
	size_t iters = 100;
	for (size_t counter = 0; counter < iters; ++counter) {
		sut.perform_move(5);
		if (counter % 7) {
			sut.save_optimal();
		}
		save_map_as_image("simple_mover_saving");
		map = original_map;
		std::cerr <<counter << std::endl;
	}
}



