#ifndef ABSTRACT_OPTIMIZABLE_STUB_H
#define ABSTRACT_OPTIMIZABLE_STUB_H

#include <abstract_optimizable.h>

class abstract_optimizable_stub : public abstract_optimizable
{
public:
    double cost() const override;
    void next_state(double temperature);
    void save_optimal_state();
    double optimal_state() { return optimal_x; }
protected:
    double x = 0;
    double optimal_x = 0;

	// abstract_optimizable interface
public:
	void request_interrupt();
};

#endif // ABSTRACT_OPTIMIZABLE_STUB_H

