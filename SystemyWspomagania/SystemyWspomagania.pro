TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    simmulated_anneling.cpp \
    simmulated_anneling_test.cpp \
    lisflood.cpp \
    lisflood_test.cpp \
    abstract_optimizable_stub.cpp \
    environment_map.cpp \
    field.cpp \
    field_test.cpp \
    map_reader.cpp \
    map_reader_test.cpp \
    river_reader_test.cpp \
    river_reader.cpp \
    simple_mover.cpp \
    two_sided_mover.cpp \
    module_tests.cpp \
    simple_mover_test.cpp \
    iterative_mover.cpp \
    iterative_mover_test.cpp \
    two_sided_mover_test.cpp

QMAKE_CXXFLAGS -= -O2
QMAKE_CXXFLAGS -= -O1
QMAKE_CXXFLAGS += --std=c++11 -Werror -O3 -m64

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    simmulated_anneling.h \
    abstract_optimizable.h \
    abstract_optimizable_mock.h \
    lisflood.h \
    environment_map.h \
    abstract_optimizable_stub.h \
    field.h \
    map_reader.h \
    river_reader.h \
    abstract_sandbag_mover.h \
    simple_mover.h \
    two_sided_mover.h \
    iterative_mover.h

LIBS += -lgmock -lpthread -lprotobuf -lboost_filesystem -lboost_system -lpng

DISTFILES +=
