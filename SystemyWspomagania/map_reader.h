#ifndef MAP_READER_H
#define MAP_READER_H

#include <png++/image.hpp>
#include <png++/image_info.hpp>
#include <boost/filesystem.hpp>
#include <map>
#include <set>

namespace png {
bool operator<(const rgb_pixel& a, const rgb_pixel& b);
bool operator>(const rgb_pixel& a, const rgb_pixel& b);
bool operator==(const rgb_pixel& a, const rgb_pixel& b);
bool operator!=(const rgb_pixel& a, const rgb_pixel& b);
}

using path = boost::filesystem::path;
class map_reader
{
public:
	/*!
	 * \brief map_reader
	 * \param legend_dir - directory containing legend files with names equal to height being
	 *        represented by color of the file.
	 * \param map_path - path to png file containing map
	 */
	map_reader(const path& legend_dir, const path& map_path);

	/*!
	 * \brief default destructor
	 */
	~map_reader() = default;

	/*!
	 * \brief read_map_file - converts png file to DEM
	 * \return returns two-dimensional vector of each pixel height
	 */
	std::vector<std::vector<size_t> > read_map_file();

	/*!
	 * \brief convert_to_height - converts given pixel to corresponding height
	 * \param pix - pixel in RGB format to be converted
	 * \return height of the pixel - \see constructor
	 */
	double convert_to_height(const png::rgb_pixel& pix) const;

	/*!
	 * \brief convert_to_pix - converts height back to the pixel format
	 * \param height - height to be converted
	 * \return pixel representing given height
	 */
	png::rgb_pixel convert_to_pix(double height);
private:
	void read_legend_file_list();
	void read_legend_files();
	size_t filename_to_height(const path& pt) const;

private:
	path legend_dir;
	path map_path;
	std::set<path> legend_files;
	std::map<png::rgb_pixel, size_t> conversion_map;
	std::map<size_t, png::rgb_pixel> reverse_conv_map;
	std::vector<std::vector<double>> terrain_map;
};

#endif // MAP_READER_H
