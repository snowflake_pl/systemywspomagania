#ifndef ABSTRACT_SANDBAG_MOVER_H
#define ABSTRACT_SANDBAG_MOVER_H

#include <environment_map.h>
#include <stddef.h>

class abstract_sandbag_mover
{
public:
	virtual ~abstract_sandbag_mover() {}

	/*!
	 * \brief perform_move - computes new permutation of sandbags distribution
	 * \param temperature - process parameter - describes range of search
	 */
	virtual void perform_move(double temperature) = 0;

	/*!
	 * \brief save_optimal - saves current state as optimal for next iterations
	 */
	virtual void save_optimal() = 0;

protected:
	size_t next_point_finder(const double weir_center,
	                         const double size,
	                         const double temperature,
	                         std::default_random_engine& engine) const
	{
		double temp = temperature;
		double probability = weir_center / size;
		std::binomial_distribution<int> binominal_dist(size, probability);
		int los = binominal_dist(engine);
		int ret(0);
		int offset = (los - weir_center);

		size_t counter = 10;
		do {
			int scaled_offset = offset * temp;
			temp /= 2.0;
			ret = los + scaled_offset;
			--counter;
		} while (((ret < 0) || (ret >= size)) && counter > 0);

		return (ret < size) ? (ret >= 0 ? ret : 0) : size -1;

		//temperature brings random new point closer to current point
		//it gives the effect of "cooldown" in simmulated annealing
	}
};



#endif // ABSTRACT_SANDBAG_MOVER_H

