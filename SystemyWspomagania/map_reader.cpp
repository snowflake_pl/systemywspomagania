#include "map_reader.h"

#include <png++/image.hpp>
#include <boost/filesystem.hpp>
#include <iostream>

map_reader::map_reader(const path& legend_dir, const path& map_path) :
    legend_dir(legend_dir),
    map_path(map_path)
{
	read_legend_file_list();
	read_legend_files();
}

namespace fs = boost::filesystem;

std::ostream& operator<<(std::ostream& out, const png::rgb_pixel& px)
{
	return out <<"(" <<static_cast<int>(px.red)
	           <<", "<<static_cast<int>(px.green)
	           <<", "<<static_cast<int>(px.blue)<<") ";
}

void map_reader::read_legend_file_list()
{
	fs::recursive_directory_iterator end;
	fs::recursive_directory_iterator  dir_iter(legend_dir);
	for ( ;dir_iter != end; ++dir_iter) {
		legend_files.insert(*dir_iter);
	}
}

inline
size_t map_reader::filename_to_height(const path& pt) const
{
	return std::stoi(pt.stem().string());
}

void map_reader::read_legend_files()
{
	for (const path& pt : legend_files) {
		png::image< png::rgb_pixel > image(pt.c_str());
		const png::rgb_pixel pixel = image.get_pixel(0,0);
		const size_t h = filename_to_height(pt);
		conversion_map[pixel] = h;
		reverse_conv_map[h] = pixel;
	}
}

std::vector<std::vector<size_t>> map_reader::read_map_file()
{
	png::image< png::rgb_pixel > image(map_path.c_str());

	std::vector<std::vector<size_t>> map;

	for (size_t i = 0; i < image.get_width(); ++i) {
		map.push_back(std::vector<size_t>());
		for (size_t j = 0; j < image.get_height(); ++j) {
			auto h = convert_to_height(image.get_pixel(i, j));
			map[i].push_back(h);
		}
	}
	return std::move(map);
}

png::rgb_pixel map_reader::convert_to_pix(double height)
{
	return reverse_conv_map.lower_bound(static_cast<size_t>(height))->second;
}

double map_reader::convert_to_height(const png::rgb_pixel& pix) const
{
	if (conversion_map.empty()) {
		return 0;
	}
	auto it = conversion_map.lower_bound(pix);
	if (it == conversion_map.end()) {
		return conversion_map.rbegin()->second;
	}
	return it->second;
}

namespace png {
bool operator<(const rgb_pixel& a, const rgb_pixel& b)
{
	return a.red < b.red ||
	        (a.red == b.red && a.green < b.green) ||
	        (a.red == b.red && a.green == b.green && a.blue < b.blue );
}

bool operator==(const rgb_pixel& a, const rgb_pixel& b)
{
	return (a.blue == b.blue && a.green == b.green && a.red == b.red);
}

bool operator!=(const rgb_pixel& a, const rgb_pixel& b)
{
	return !(a == b);
}

bool operator>(const rgb_pixel& a, const rgb_pixel& b)
{
	return (a != b) && !(a < b);
}
} // namespace png

