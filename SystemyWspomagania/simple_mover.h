#ifndef SIMPLE_MOVER_H
#define SIMPLE_MOVER_H

#include <abstract_sandbag_mover.h>
#include <random>

class simple_mover : public abstract_sandbag_mover
{
public:
	simple_mover(const size_t sandbag_count, environment::map& map,
	             const std::vector<std::pair<size_t, size_t>>& river,
	             long double weir_height);

	/*!
	 * \copydoc abstract_sandbag_mover::perform_move
	 */
	void perform_move(double temperature) override;

	/*!
	 * \copydoc abstract_sandbag_mover::save_optimal
	 */
	void save_optimal() override;

protected:
	void find_new_weir_center(double temperature);
	void calculate_weir_dimmensions();
	void embant_weir_center_from_all_sides();
	void embankt_left_of(environment::field& field);
	void embankt_right_of(environment::field& field);
	void embankt_bottom_of(environment::field& field);
	void embankt_top_of(environment::field& field);
	void embankt(environment::field& field);
	void embankt_field(environment::field& field);
	void embankt_field_from_top_and_left(const environment::field& field);
	void build_weir();
	void reset_counters();
	bool can_continue() const;
	void try_building_down_the_river(size_t& bottom_left_index);
	void try_building_up_the_river(size_t& top_right_index);
private:
	size_t sandbag_count;
	environment::map& map;
	std::uniform_int_distribution<size_t> randomizer;
	size_t weir_center;
	size_t optimal_center;
	size_t number_of_fields_to_cover;
	size_t cells_per_side;
	const std::vector<std::pair<size_t, size_t>>& river_fields;
	long double required_weir_height;
	size_t fields_embanket;
	size_t x, y;
	std::tuple<size_t&, size_t&> unwrapper;
	bool can_build_more_up;
	bool can_build_more_down;
};

#endif // SIMPLE_MOVER_H
