#ifndef FIELD_H
#define FIELD_H

#include <stddef.h>

namespace environment {

class field
{
public:

	//constructors
	field();
	field(size_t x,
	      size_t y,
	      double value,
	      double height,
	      double sand_height,
	      double water_volume,
	      bool is_river = false);
	~field() = default;

	//setters and getters
	void set_value(double val);
	long double value() const;
	void add_water(long double volume_to_add);
	void set_sand_height(long double height);
	double height() const;
	double water_height() const;
	bool is_flooded() const;
	double sand_height() const;
public:
	size_t x_;
	size_t y_;
	bool is_river;
	static constexpr long double dx{10};
	static constexpr long double dy{10};
	static constexpr long double cell_area{dx*dy};
private:
	double value_;
	double height_;
	double sand_height_;
	double water_volume_;
};

} // namespace environment

#endif // FIELD_H
