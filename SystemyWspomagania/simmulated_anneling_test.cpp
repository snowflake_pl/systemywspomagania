#include <gtest/gtest.h>


#include <abstract_optimizable_mock.h>
#include <simmulated_anneling.h>
#include <abstract_optimizable_stub.h>

long double x=0;
long double increment=1;
long double optimal_x = x;

void tested_next_step(long double)
{
    ++x;
}

void tested_revert_step()
{
    optimal_x = x;
}

long double tested_cost_function()
{
    return (x-10)*(x);
}


class simmulated_anneling_test : public ::testing::Test
{
public:
    simmulated_anneling_test() :
        process(new abstract_optimizable_stub()),
        sut(process)
    {}
protected:
    std::shared_ptr<abstract_optimizable> process;
    simmulated_anneling sut;
};

using namespace ::testing;

TEST_F(simmulated_anneling_test, algorithm_find_minimum_of_simple_function)
{
    sut.run();
    auto process = std::static_pointer_cast<abstract_optimizable_stub>(this->process);
    const double epsilon = 0.001;
    EXPECT_NEAR(-39.16616, process->cost(), epsilon);
    EXPECT_NEAR(-2.903534, process->optimal_state(), epsilon);
}


