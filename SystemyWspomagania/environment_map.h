#ifndef ENVIRONMENT_MAP_H
#define ENVIRONMENT_MAP_H

#include <vector>
#include <iostream>
#include <boost/optional.hpp>

namespace environment {

class field;

using row = std::vector<field>;
using row_iterator = std::vector<row>::iterator;
using row_const_iterator = std::vector<row>::const_iterator;

class map
{
public:
	//constructors
	map(std::initializer_list<row> list);
	map(size_t count);
	map(size_t count, row&& row_);
	map(const std::vector<std::vector<size_t>>& heights);

	//STL compliant interface
	field at(size_t i, size_t j) const;
	row& operator[](size_t i);
	row_iterator begin();
	row_iterator end();
	row_const_iterator begin() const;
	row_const_iterator end() const;
	size_t size() const;
	bool empty() const;
	void push_back(row&& row_);

	//usefull getters
	size_t width() const;
	size_t height() const;

	//this methods return right/left/etc fields from the given one
	field left_of(const field& f) const;
	field right_of(const field& f) const;
	field above(const field& f) const;
	field  below(const field& f) const;

	//non-const versions of the above
	field& operator [](const field& fld);
	field& left_of(const field& f);
	field& right_of(const field& f);
	field& above(const field& f);
	field& below(const field& f);

	//utilities
	void add_river(const std::vector<std::pair<size_t, size_t>>& river, double water_volume);
	void add_values(std::vector<std::vector<size_t> >&& values);
private:
	std::vector<row> map_;
	bool is_on_left_edge(const field& f) const;
	bool is_on_right_edge(const field& f) const;
	bool is_on_top(const field& f) const;
	bool is_at_the_bottom(const field& f) const;

};

} // namespace environment

#endif // ENVIRONMENT_MAP_H

