#ifndef TWO_SIDED_MOVER_H
#define TWO_SIDED_MOVER_H

#include <abstract_sandbag_mover.h>
#include <random>
#include <map>

class two_sided_mover : public abstract_sandbag_mover
{
public:
	two_sided_mover(size_t sandbag_count,
	                environment::map& map,
	                const std::vector<std::pair<size_t, size_t> >& river,
	                long double weir_height);

public:

	/// \see abstract_sandbag_mover
	void perform_move(double temperature) override;

	/// \see abstract_sandbag_mover
	void save_optimal() override;
private:
	void embankt_weir_center(size_t center, bool right);
	void try_building_up(size_t& top_right_index, bool right);
	void try_building_downward(size_t& bottom_left_index, bool right);
	size_t find_new_weir_center(size_t center, double temperature);
	size_t calculate_weir_dimmensions(size_t sandbag_count);
	void reset_counters();
	void embankt_top_of(environment::field& field);
	void embankt_bottom_of(environment::field& field);
	void embankt_right_of(environment::field& field);
	void embankt_left_of(environment::field& field);
	void embankt(environment::field& field, bool right);
	void embankt_field(environment::field& field, bool right);
	void build_weir(size_t center, bool right);
	bool can_continue(bool right) const;
	bool can_build_up_more_cells(bool right) const;
private:
	std::map<bool, size_t> sandbag_count;
	environment::map& map;
	std::vector<std::pair<size_t, size_t>> river;
	std::uniform_int_distribution<size_t> randomizer;
	std::map<bool, size_t> center;
	std::map<bool, size_t> optimal;
	long double required_weir_height;
	std::map<bool, size_t> cells_to_cover;
	std::map<bool, size_t> fields_embanket;
	size_t x, y;
	std::tuple<size_t&, size_t&> unwrapper;
	std::map<bool, bool> can_go_higher;
	std::map<bool, bool> can_go_lower;
};

#endif // TWO_SIDED_MOVER_H
