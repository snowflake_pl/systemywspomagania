#include <gtest/gtest.h>

#include <lisflood.h>
#include <simple_mover.h>
#include <environment_map.h>
#include <field.h>
#include <river_reader.h>
#include <map_reader.h>

using std::placeholders::_1;
class lisflood_test : public ::testing::Test
{
public:
	lisflood_test() :
	    river(10),
	    map{1000},
	    sandbag_count(100),
	    temp_mover(new simple_mover(sandbag_count, map, river, 2.0)),
	    mapper("./legend","./mapa_podniesiona.png"),
	    sut(std::move(temp_mover), map, river,
	        std::bind(&map_reader::convert_to_pix, &mapper, _1),
	        std::bind(&map_reader::convert_to_height, &mapper, _1), 10, 100,
	        {})
	{
	}

protected:
	std::vector<std::pair<size_t, size_t>> river;
	environment::map map;
	size_t sandbag_count;
	std::unique_ptr<abstract_sandbag_mover> temp_mover;
	map_reader mapper;
	lisflood sut;
};

using namespace ::testing;

TEST_F(lisflood_test, sanity_check)
{
	const double iters = 100;
	for (size_t i = 0; i < iters; ++i) {
		sut.cost();


		for (int k = 0; k < 10; k++) {
			sut.next_state(1 - (i / iters / 2.0));
		}
	}
}
