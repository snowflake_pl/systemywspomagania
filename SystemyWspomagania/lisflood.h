#ifndef FISFLOOD_H
#define FISFLOOD_H

#include <vector>
#include <stddef.h>
#include <memory>

#include <abstract_optimizable.h>
#include <environment_map.h>
#include <map_reader.h>
#include <png++/image.hpp>
#include <atomic>

class abstract_sandbag_mover;

class lisflood : public abstract_optimizable
{
public:
	lisflood(std::unique_ptr<abstract_sandbag_mover> mover,
	         environment::map& map,
	         const std::vector<std::pair<size_t, size_t>>& riv,
	         std::function<png::rgb_pixel(size_t)> pixel_from_height,
	         std::function<double(const png::rgb_pixel&)> height_from_pixel,
	         double water_volume, size_t iterations,
	         std::vector<std::vector<size_t>>&& values);
	~lisflood();

	/// \see abstract_optimizable
	double cost() const override;

	/// \see abstract_optimizable
	void next_state(double temperature) override;

	/// \see abstract_optimizable
	void save_optimal_state() override;

	/// \see abstract_optimizable
	void request_interrupt() override;
private:
	void calculate_flow();
	double delta_volume(const environment::field& f) const;
	double hflow(const environment::field& f1, const environment::field& f2) const;
	double Q(const environment::field& f1, const environment::field& f2) const;
	void print_map();
	double direction(const environment::field & f1, const environment::field & f2) const;
	double flow_limmiter(long double res,
	                     const environment::field& f1,
	                     const environment::field& f2) const;
	double friction_slope(const environment::field& f1, const environment::field& f2) const;
	double water_slope(const environment::field& f1, const environment::field& f2) const;
	void interrupt();
private:
	size_t remaining_bags;
	environment::map& map;
	environment::map optimal_state;
	environment::map initial_map;
	std::function<png::rgb_pixel(size_t)> pixel_from_height;
	std::function<double(const png::rgb_pixel&)> height_from_pixel;
	std::unique_ptr<abstract_sandbag_mover> mover;
	std::vector<std::pair<size_t, size_t>> river;
	void save_map_as_image(const std::string& filename);
	png::rgb_pixel height_to_pixel(const environment::field& f);
	std::fstream file;
	size_t iterations;
	mutable double cost_;
	std::atomic<bool> interrupt_requested;
};

#endif // FISFLOOD_H
