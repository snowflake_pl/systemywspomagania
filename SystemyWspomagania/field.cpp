#include "field.h"
#include <stddef.h>
#include <cmath>
#include <iostream>
namespace environment {

namespace constant {
constexpr long double flood_threshold = 0;//.001;
} // namespace constant


field::field() : field(0, 0, 0, 0, 0, 0)
{
}

field::field(size_t x,
             size_t y,
             double value,
             double height,
             double sand_height,
             double water_volume,
             bool is_river) :
    x_(x),
    y_(y),
    is_river(is_river),
    value_(value),
    height_(height),
    sand_height_(sand_height),
    water_volume_(water_volume)
{
}

void field::set_value(double val)
{
	value_ = val;
}

void field::add_water(long double volume_to_add)
{
	if (is_river && std::isless(volume_to_add, 0.0)) {
		return; //can only add water to the river
	}
	water_volume_ += volume_to_add;
}

void field::set_sand_height(long double height)
{
	sand_height_ = height;
}

long double field::value() const
{
    return is_flooded() ? value_ : 0.0;
}

double field::height() const
{
	return height_ + sand_height_;
}

double field::water_height() const
{
	return height() + (water_volume_ / cell_area);
}

bool field::is_flooded() const
{
    return std::isgreater((water_height() - height()), constant::flood_threshold);
}

double field::sand_height() const
{
	return sand_height_;
}

} // namespace environment
