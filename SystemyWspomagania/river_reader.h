#ifndef RIVER_READER_H
#define RIVER_READER_H

#include <boost/filesystem.hpp>
#include <vector>
#include <tuple>

using path = boost::filesystem::path;


class river_reader
{
public:
	/*!
	 * \brief river_reader
	 * \param file_path - path to file with the river
	 */
	river_reader(const path& file_path);

	/*!
	 * \brief get_river_map - returns sequene of fields that are part of the river
	 * \return vector[n] of pairs containing x (first) and y (second) coordinate
	 *         of n'th field of the river
	 */
	std::vector<std::pair<size_t, size_t> > get_river_map() const;
private:
	path river_path;

	std::vector<std::vector<bool>> riverness_map;

	std::vector<std::pair<size_t, size_t>> river_fields;
};

#endif // RIVER_READER_H
