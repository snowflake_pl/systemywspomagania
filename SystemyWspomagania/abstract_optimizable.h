#ifndef ABSTRACT_OPTIMIZABLE_H
#define ABSTRACT_OPTIMIZABLE_H

class abstract_optimizable
{
public:
	/*!
	 * \brief Function used to calculate cost of current state
	 * \return double value of cost at current state of process
	 */
	virtual double cost() const = 0;

	/*!
	 * \brief Function used to move process to the next tested state
	 * \param temperature - energy to be used in generating next state
	 */
	virtual void next_state(double temperature) = 0;

	/*!
	 * \brief This function shall save current state of the process to internal variable that will
	 * be returned after the optimization procedure is finished
	 */
	virtual void save_optimal_state() = 0;

	/*!
	 * \brief ~abstract_optimizable - the destructor
	 */
	virtual ~abstract_optimizable() {}

	/*!
	 * \brief request_interrupt - sets interruption point to stop the thread
	 */
	virtual void request_interrupt() = 0;
};

#endif // ABSTRACT_OPTIMIZABLE_H

