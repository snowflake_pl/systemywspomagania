#include "simmulated_anneling.h"

#include <iostream>

#include <abstract_optimizable.h>
#include <cmath>
#include <random>

namespace consts {
constexpr size_t tries_per_T = 20;      //describes tries per temperature (algoritm parameter)
constexpr double starting_T = 5.0;      //describes starting temperature (algoritm parameter)
constexpr double minimal_t = 0.25;       //describes lower bound of temperature (algoritm parameter)
constexpr double cooling_factor = 0.5;  //describes speed of cooldown (algoritm parameter)
constexpr double eulers = 2.7182818284590452354;
constexpr double boltzmans = 0.00086173324;

std::random_device random_device;
std::default_random_engine random_engine(random_device());
std::uniform_real_distribution<double> randomizer;
} // namespace consts

simmulated_anneling::simmulated_anneling(std::shared_ptr<abstract_optimizable> process) :
    process(process)
{
	if (!process) {
		throw std::runtime_error("Recieved nullptr as process");
	}
}

void simmulated_anneling::run()
{
	double current_cost = create_starting_state();

	for (double T = consts::starting_T;
	     T >= consts::minimal_t;
	     T *= consts::cooling_factor) {
		look_for_optimal_state(current_cost, T);
	}
}

double simmulated_anneling::create_starting_state()
{
	process->next_state(consts::starting_T);
	process->save_optimal_state();
	return process->cost();
}

void simmulated_anneling::look_for_optimal_state(double& current_cost, double T)
{
	for (size_t tr = 0; tr < consts::tries_per_T; ++tr) {
		double new_cost = cost_of_next_state(T);
		if (should_accept_current_solution(new_cost, current_cost, T)) {
			accept_current_solution_as_optimal(current_cost, new_cost);
		}
	}
}

double simmulated_anneling::cost_of_next_state(double T)
{
	process->next_state(T);
	return process->cost();
}

bool simmulated_anneling::should_accept_current_solution(double new_cost,
                                                         double current_cost,
                                                         double T) const
{
	return std::isless(new_cost, current_cost) ||
	        std::exp((current_cost - new_cost)/(consts::boltzmans * T)) > random();
}

double simmulated_anneling::random() const
{
	return consts::randomizer(consts::random_device);
}

void simmulated_anneling::accept_current_solution_as_optimal(double& current_cost, double new_cost)
{
	process->save_optimal_state();
	current_cost = new_cost;
}


