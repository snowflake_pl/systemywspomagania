#include <gtest/gtest.h>

#include <map_reader.h>
#include <boost/assign/list_of.hpp>

struct test_param
{
	path p;
	double h;
};

class map_reader_test : public testing::TestWithParam<test_param>
{
public:
	map_reader_test() :
	    reader("./legend", "./mapa_terenu.png")
	{}

protected:
	map_reader reader;
};

TEST_P(map_reader_test, can_convert_pixels_from_legend_to_themselves)
{
	const path p = path("./legend") / GetParam().p;
	const double expected_height = GetParam().h;
	png::image< png::rgb_pixel > image(p.c_str());
	EXPECT_EQ(expected_height, reader.convert_to_height(image.get_pixel(0,0)));
}

TEST_F(map_reader_test, reads_map_file)
{
	reader.read_map_file();
}

TEST(water_reader_test, reading_water)
{
	map_reader reader("./legend", "./woda.png");
}

std::vector<test_param> test_parameters = boost::assign::list_of
                                          (test_param{"105.png", 105})
                                          (test_param{"110.png", 110})
                                          (test_param{"111.png", 111})
                                          (test_param{"112.png", 112})
                                          (test_param{"113.png", 113})
                                          (test_param{"114.png", 114})
                                          (test_param{"115.png", 115})
                                          (test_param{"116.png", 116})
                                          (test_param{"117.png", 117})
                                          (test_param{"118.png", 118})
                                          (test_param{"119.png", 119})
                                          (test_param{"120.png", 120})
                                          (test_param{"121.png", 121})
                                          (test_param{"122.png", 122})
                                          (test_param{"123.png", 123})
                                          (test_param{"124.png", 124})
                                          (test_param{"125.png", 125})
                                          (test_param{"126.png", 126})
                                          (test_param{"127.png", 127})
                                          (test_param{"128.png", 128})
                                          (test_param{"129.png", 129})
                                          (test_param{"130.png", 130})
                                          (test_param{"131.png", 131})
                                          (test_param{"132.png", 132})
                                          (test_param{"133.png", 133})
                                          (test_param{"134.png", 134})
                                          (test_param{"135.png", 135})
                                          (test_param{"136.png", 136})
                                          (test_param{"137.png", 137})
                                          (test_param{"138.png", 138})
                                          (test_param{"140.png", 140})
                                          (test_param{"142.png", 142})
                                          (test_param{"145.png", 145})
                                          (test_param{"148.png", 148})
                                          (test_param{"152.png", 152})
                                          (test_param{"156.png", 156});

INSTANTIATE_TEST_CASE_P(map_reader_test, map_reader_test, ::testing::ValuesIn(test_parameters));
