#include <gtest/gtest.h>

#include <environment_map.h>
#include <map_reader.h>
#include <river_reader.h>
#include <field.h>
#include <simmulated_anneling.h>
#include <simple_mover.h>
#include <two_sided_mover.h>
#include <lisflood.h>

#include <boost/assign/list_of.hpp>

struct test_data
{
	size_t sb;
	double wh;
	size_t it;
};

using std::placeholders::_1;
class mt_project_test : public ::testing::TestWithParam<test_data>
{
public:
	mt_project_test() :
	    sandbags(GetParam().sb),
	    riv("./rzeka_mini.png"),
	    river(riv.get_river_map()),
	    mapper("./legend","./mapa_mini.png"),
	    valuer("./values", "./one_value.png"),
	    mapa(mapper.read_map_file()),
	    mover(new two_sided_mover(sandbags, mapa, river, GetParam().wh)),
	    flood(std::make_shared<lisflood>(std::move(mover), mapa, river,
	                                     std::bind(&map_reader::convert_to_pix, &mapper, _1),
	                                     std::bind(&map_reader::convert_to_height, &mapper, _1),
	                                     GetParam().wh, GetParam().it,
	                                     valuer.read_map_file())),
	    optimizer(flood)
	{}
protected:
	size_t sandbags;
	river_reader riv;
	std::vector<std::pair<size_t, size_t>> river;
	map_reader mapper;
	map_reader valuer;
	environment::map mapa;
	std::unique_ptr<abstract_sandbag_mover> mover;
	std::shared_ptr<abstract_optimizable> flood;
	simmulated_anneling optimizer;
};

TEST_P(mt_project_test, sanity_check)
{
	optimizer.run();
	EXPECT_EQ(GetParam().sb, sandbags);
}



std::vector<test_data> params{
//	{20000, 10.0, 10},
//	{20000, 10.0, 100},
//	{20000, 10.0, 500},
//	{200000, 100.0, 1000},
	{80000, 0, 10000}
//	{20000, 10.0, 2000}
};

INSTANTIATE_TEST_CASE_P(mt_project_test_instance, mt_project_test,
                        ::testing::ValuesIn(params));
