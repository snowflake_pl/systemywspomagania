#ifndef ABSTRACT_OPTIMIZABLE_MOCK_H
#define ABSTRACT_OPTIMIZABLE_MOCK_H

#include <gmock/gmock.h>

#include <abstract_optimizable.h>

class abstract_optimizable_mock : public abstract_optimizable {
public:
    MOCK_CONST_METHOD0(cost, double());
    MOCK_METHOD1(next_state, void(double));
    MOCK_METHOD0(save_optimal_state, void());
};

#endif // ABSTRACT_OPTIMIZABLE_MOCK_H

