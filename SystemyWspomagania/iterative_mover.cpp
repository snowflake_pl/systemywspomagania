#include "iterative_mover.h"
#include <field.h>


namespace constant {
constexpr double sandbag_height = 0.5; // meter
constexpr double sandbag_length = 1.0;// meter
constexpr double cell_side = environment::field::dx;
constexpr double stacks_per_side = (cell_side / sandbag_length);
} // namespace constant

namespace {
size_t bags_per_cell(double weir_height)
{
	size_t stack_size = std::ceil(weir_height/constant::sandbag_height);
	return stack_size * constant::stacks_per_side;
}
} // namespace unnamed

iterative_mover::iterative_mover(const size_t sandbag_count, environment::map& map,
                                 const std::vector<std::pair<size_t, size_t>>& river,
                                 long double weir_height, const size_t step) :
    sandbag_count(sandbag_count),
    map(map),
    weir_center(0),
    optimal_center(weir_center),
    number_of_fields_to_cover(0),
    cells_per_side(0),
    river_fields(river),
    required_weir_height(weir_height),
    fields_embanket(0),
    unwrapper(std::tie(x, y)),
    can_build_more_up(true),
    can_build_more_down(true),
    step(step)
{

}

void iterative_mover::perform_move(double temperature)
{
	if (required_weir_height == 0) {
		return;
	}
	reset_counters();
	find_new_weir_center(temperature);
	calculate_weir_dimmensions();
	build_weir();
}

void iterative_mover::calculate_weir_dimmensions()
{
	number_of_fields_to_cover = sandbag_count / bags_per_cell(required_weir_height);
}

void iterative_mover::build_weir()
{
	embant_weir_center_from_all_sides();

	size_t top_right_index = weir_center + 1;
	size_t bottom_left_index = weir_center;

	while (can_continue()) {
		try_building_up_the_river(top_right_index);
		try_building_down_the_river(bottom_left_index);
	}
}

void iterative_mover::embant_weir_center_from_all_sides()
{
	unwrapper = river_fields.at(weir_center);
	embankt_field(map[x][y]);
}

bool iterative_mover::can_continue() const
{
	return (fields_embanket < number_of_fields_to_cover)
	        && (can_build_more_up || can_build_more_down);
}

void iterative_mover::try_building_up_the_river(size_t& top_right_index)
{
	if (top_right_index < river_fields.size()) {
		unwrapper = river_fields.at(top_right_index++);
		embankt_field(map[x][y]);
	} else {
		can_build_more_up = false;
	}
}

void iterative_mover::try_building_down_the_river(size_t& bottom_left_index)
{
	if (bottom_left_index) {
		unwrapper= river_fields.at(--bottom_left_index);
		embankt_field(map[x][y]);
	} else {
		can_build_more_down = false;
	}
}

void iterative_mover::embankt_field(environment::field& field)
{
	embankt_top_of(field);
	embankt_bottom_of(field);
	embankt_right_of(field);
	embankt_left_of(field);
}

void iterative_mover::embankt_left_of(environment::field& field)
{
	embankt(map.left_of(field));
}

void iterative_mover::embankt_right_of(environment::field& field)
{
	embankt(map.right_of(field));
}

void iterative_mover::embankt_bottom_of(environment::field& field)
{
	embankt(map.below(field));
}

void iterative_mover::embankt_top_of(environment::field& field)
{
	embankt(map.above(field));
}

void iterative_mover::embankt(environment::field& field)
{
	if (field.is_river || fields_embanket >= number_of_fields_to_cover) {
		return;
	}
	++fields_embanket;
	field.set_sand_height(required_weir_height);
}

void iterative_mover::reset_counters()
{
	fields_embanket = 0;
	can_build_more_down = can_build_more_up = true;
}

void iterative_mover::save_optimal()
{
	optimal_center = weir_center;
}

void iterative_mover::find_new_weir_center(double /*temperature*/)
{
	auto nc = weir_center + step;
	weir_center = nc < river_fields.size() ? nc : river_fields.size() - 1;
}
