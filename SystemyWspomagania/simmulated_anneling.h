#ifndef SIMMULATED_ANNELING_H
#define SIMMULATED_ANNELING_H

#include <memory>
#include <functional>

class abstract_optimizable;


class simmulated_anneling
{
public:

	/*!
	 * \brief simmulated_anneling
	 * \param process - shared pointer to the process being optimized -
	 *        the process must implement abstract_optimizible interface
	 */
	simmulated_anneling(std::shared_ptr<abstract_optimizable> process);

	/// \brief default destructor
	~simmulated_anneling() = default;

	/*!
	 * \brief run - performs optimization
	 */
	void run();

private:
	double create_starting_state();
	void look_for_optimal_state(double& current_cost, double T);
	double cost_of_next_state(double T);
	void accept_current_solution_as_optimal(double& current_cost, double new_cost);
	bool should_accept_current_solution(double, double, double T) const;
	double random() const;
private:
	std::shared_ptr<abstract_optimizable> process;
};

#endif // SIMMULATED_ANNELING_H
